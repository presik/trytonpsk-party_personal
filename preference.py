# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval

STATES = {
    'readonly': ~Eval('active'),
}
DEPENDS = ['active']


class Preference(ModelSQL, ModelView):
    "Party Preference"
    __name__ = 'party.preference'
    name = fields.Char('Name', required=True, states=STATES, translate=True,
        depends=DEPENDS)
    parent = fields.Many2One('party.preference', 'Parent',
        states=STATES, depends=DEPENDS)
    childs = fields.One2Many('party.preference', 'parent',
       'Children', states=STATES, depends=DEPENDS)
    active = fields.Boolean('Active')

    @staticmethod
    def default_active():
        return True
