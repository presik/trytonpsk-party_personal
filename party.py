# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, datetime

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard

# Size photo default
WIDTH = 200
HEIGHT = 250
STATES = {
    'readonly': ~Eval('active', True),
}
DEPENDS = ['active']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    birthday = fields.Date('Birthday')
    age = fields.Function(fields.Integer('Age'), 'get_age')
    # city_born = fields.Char('City Born')
    education_level = fields.Selection([
        ('elementary', 'Elementary'),
        ('secondary', 'Secondary'),
        ('intermedium', 'Intermedium'),
        ('vocational', 'Vocational'),
        ('universitary', 'Universitary'),
        ('postgraduate', 'Postgraduate'),
        ('', ''),
        ], 'Education Level')
    education_level_string = education_level.translated('education_level')
    sex = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('', ''),
        ], 'Sex')
    sex_string = sex.translated('sex')
    marital_status = fields.Selection([
        ('free_union', 'Free Union'),
        ('married', 'Married'),
        ('single', 'Single'),
        ('divorced', 'Divorced'),
        ('widow', 'Widow'),
        ('', ''),
        ], 'Marital Status')
    marital_status_string = marital_status.translated('marital_status')
    num_children = fields.Integer('No. Children')
    education = fields.Char('Education')
    profession = fields.Char('Profession')
    responsible_person = fields.Char('Responsible Person')
    phone_responsible_person = fields.Char('Phone Responsible Person')
    nationality = fields.Many2One('country.country', 'Nationality')
    health = fields.Text('Health')
    notes = fields.Text('Notes')
    weight = fields.Float('Weight')
    bmi = fields.Float('Body Mass Index')
    bmi_category = fields.Selection([
            ('', ''),
            ('1', 'Underweight'),
            ('2', 'Normal'),
            ('3', 'Overweight'),
            ('4', 'Obese Class I'),
            ('5', 'Obese Class II'),
            ('6', 'Obese Class III'),
        ], 'BMI Category', states={'readonly': True})
    blood_group = fields.Selection([
            ('', ''),
            ('rh_op', 'Rh O+'),
            ('rh_on', 'Rh O-'),
            ('rh_ap', 'Rh A+'),
            ('rh_an', 'Rh A-'),
            ('rh_bp', 'Rh B+'),
            ('rh_bn', 'Rh B-'),
            ('rh_abp', 'Rh AB+'),
            ('rh_abn', 'Rh AB-'),
            ], 'Blood Group')
    blood_group_string = blood_group.translated('blood_group')
    preferences = fields.Many2Many('party.party-party.preference',
        'party', 'preference', 'Preferences', states=STATES, depends=DEPENDS)
    first_name = fields.Char('Primer Nombre')
    second_name = fields.Char('Segundo Nombre')
    first_family_name = fields.Char('Primer Apellido')
    second_family_name = fields.Char('Segundo Apellido')
    city_document_exp = fields.Char('City of Document Expedition ID')

    @classmethod
    def validate(cls, parties):
        for party in parties:
            if party.birthday and party.birthday > date.today():
                raise UserError(gettext('party_personal.msg_birthday'))
        parties = super(Party, cls).validate(parties)
        return parties

    @fields.depends('bmi', 'bmi_category')
    def on_change_bmi(self):
        if self.bmi:
            if self.bmi < 18.5:
                category = '1'
            elif 18.5 <= self.bmi < 25:
                category = '2'
            elif 25 <= self.bmi < 30:
                category = '3'
            elif 30 <= self.bmi < 35:
                category = '4'
            elif 35 <= self.bmi < 40:
                category = '5'
            else:
                category = '6'

            self.bmi_category = category

    def get_age(self, name=None):
        if not self.birthday:
            return
        btd = self.birthday
        res = datetime.now() - datetime(btd.year, btd.month, btd.day)
        res = int(res.days / 365.25)
        return res

    """
    @classmethod
    def _resize_photo(cls, img_buff):
        # TODO: FIXME
        import Image
        from StringIO import StringIO

        #io_image = StringIO(img_buff)
        io_image = img_buff

        if io_image.len == 0:
            return None

        img = Image.open(io_image)
        width, height = img.size

        # To try center original photo for resize it
        factor_resize = float(WIDTH)/width
        height_new = int(height * factor_resize)

        # To resize photo to standar size (WIDTH, HEIGHT)
        img_resized = img.resize((WIDTH, height_new),)
        img_resized = img_resized.crop((0, 0, WIDTH, HEIGHT))

        #io_obj = StringIO()
        #img_resized.save(io_obj, 'JPEG')
        #img_data_buff = io_obj.getvalue()
        #io_obj.close()
        #return img_data_buff
        return img_resized
    """


class PartyPreference(ModelSQL):
    "Party - Preference"
    __name__ = 'party.party-party.preference'
    _table = 'party_preference_rel'
    party = fields.Many2One('party.party', 'Party', ondelete='CASCADE',
            required=True)
    preference = fields.Many2One('party.preference', 'Preference',
        ondelete='CASCADE', required=True)


class PartyByCategoryReport(Report):
    __name__ = 'party_personal.party_by_category_report'

    @classmethod
    def execute(cls, ids, data):
        oext, content, action, filename = super().execute(ids, data)
        return (oext, content, action, 'TERCEROS POR CATEGORIA')

    @classmethod
    def get_context(cls, records, header, data):
        Company = Pool().get('company.company')
        context = super().get_context(records, header, data)
        context['company'] = Company(Transaction().context.get('company'))
        context['invoice_type'] = {
            None: '',
            '': '',
            'C': 'Computador',
            'P': 'POS',
            'M': 'Manual',
            '1': 'Venta Electronica',
            '2': 'Exportacion',
            '3': 'Factura por Contingencia Facturador',
            '4': 'Factura por Contingencia DIAN',
            '91':  'Nota Crédito Eléctronica',
            '92':  'Nota Débito Eléctronica',
        }
        context['type_document'] = {
            '11': 'Registro Civil de Nacimiento',
            '12': 'Tarjeta de Identidad',
            '13': 'Cedula de Ciudadania',
            '21': 'Tarjeta de Extranjeria',
            '22': 'Cedula de Extranjeria',
            '31': 'NIT',
            '41': 'Pasaporte',
            '42': 'Tipo de Documento Extranjero',
            '47': 'PEP (Permiso Especial Permanencia)',
            '48': 'PPT (Permiso Proteccion Temporal)',
            '50': 'NIT de otro pais',
            '91': 'NUIP',
            '': '',
            None: '',
        }
        return context


class PartyByCategoryStart(ModelView):
    "Party By Category Start"
    __name__ = 'party_personal.party_by_category.start'
    categories = fields.Many2Many('party.category', None, None, 'Category')
    preferences = fields.Many2Many('party.preference', None, None, 'Preference')


class PartyByCategory(Wizard):
    "Party By Category"
    __name__ = 'party_personal.party_by_category'
    start = StateView('party_personal.party_by_category.start',
        'party_personal.party_by_category_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('party_personal.party_by_category_report')

    def do_print_(self, action):
        Party = Pool().get('party.party')
        categories_ids = []
        preferences_ids = []
        categories_names = []
        preferences_names = []
        dom = []
        for c in self.start.categories:
            categories_ids.append(c.id)
            categories_names.append(c.name + ' ')

        for p in self.start.preferences:
            preferences_ids.append(p.id)
            preferences_names.append(p.name + ' ')

        if categories_ids:
            dom.append(('categories', 'in', categories_ids))
        if preferences_ids:
            dom.append(('preferences', 'in', preferences_ids))

        parties = Party.search(dom)
        if not parties:
            raise UserError(gettext('party_personal.msg_no_parties'))
        data = {
            'ids': [p.id for p in parties],
            'categories': categories_names,
            'preferences': preferences_names,
        }
        return action, data
